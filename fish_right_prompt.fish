# Helpers
function _nexecution_time -S -d 'Return the execution time'
    [ -z "$CMD_DURATION" -o "$CMD_DURATION" -lt 100 ]
    and return

  set_color $color_path_basename

  echo -ns $left_black_arrow_glyph

  set_color -b $color_path_basename

  echo -ns '' \uFA1A

    if [ "$CMD_DURATION" -lt 5000 ]
        echo -ns $CMD_DURATION 'ms '
    else if [ "$CMD_DURATION" -lt 60000 ]
        _nformat_ms $CMD_DURATION s
    else if [ "$CMD_DURATION" -lt 3600000 ]
        _nformat_ms $CMD_DURATION m
    else
        _nformat_ms $CMD_DURATION h
    end
    set_color normal
end

function _nformat_ms -S -a ms -a interval -d 'Millisecond formatting for humans'
    set -l interval_ms
    set -l scale 1

    switch $interval
        case s
            set interval_ms 1000
        case m
            set interval_ms 60000
        case h
            set interval_ms 3600000
            set scale 2
    end

    math -s$scale "$ms/$interval_ms"
    echo -ns $interval ' '
end

# Entry point
function fish_right_prompt -d 'Noticia is about the right timing'
  _ncolors
  _nglyphs

  _nexecution_time

  set_color normal
end