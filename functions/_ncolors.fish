function _ncolors -S -a color_scheme -d 'Opinionated color defaults for Noticia'
  switch "$color_scheme"
    case '*'
      #               light  medium dark
      #               ------ ------ ------
      set -l red      cc9999 ce000f 660000
      set -l green    73D166 189303 0c4801
      set -l blue     48b4fb 005faf 255e87
      set -l orange   f6b117 unused 3a2a03
      set -l brown    bf5e00 803f00 4d2600
      set -l grey     cccccc 999999 333333
      set -l white    ffffff
      set -x black    000000
      set -l ruby_red af0000
      set -l go_blue  00d7d7

      set -x color_initial_segment_exit     $white $red[2] --bold
      set -x color_initial_segment_su       $white $green[2] --bold
      set -x color_initial_segment_jobs     $white $blue[3] --bold
      # First path segment
      set -x color_path                     $black $grey[2]
      # Second path segment
      set -x color_path_basename            $black $grey[1] #--bold
      set -x color_path_nowrite             $red[3] $red[1]
      set -x color_path_nowrite_basename    $red[3] $red[1] --bold

      set -x color_repo                     $green[1] $black
      set -x color_repo_work_tree           $grey[3] $white --bold
      set -x color_repo_dirty               $red[2] $white
      set -x color_repo_staged              $orange[1] $black --bold

      set -x color_vi_mode_default          $grey[2] $grey[3] --bold
      set -x color_vi_mode_insert           $green[2] $grey[3] --bold
      set -x color_vi_mode_visual           $orange[1] $orange[3] --bold

      set -x color_vagrant                  $blue[1] $white --bold
      set -x color_k8s                      $green[2] $white --bold
      set -x color_username                 $grey[1] $blue[3] --bold
      set -x color_hostname                 $grey[1] $blue[3]
      set -x color_rvm                      $ruby_red $grey[1] --bold
      set -x color_nvm                      $green[1] $white --bold
      set -x color_virtualfish              $blue[2] $grey[1] --bold
      set -x color_virtualgo                $go_blue $black --bold
      set -x color_desk                     $blue[2] $grey[1] --bold
      set -x color_nix                      $blue[3] $white --bold
  end
end
