function _nglyphs -S -d 'Nerd Fonts Glyphs for Noticia'
  # Defaults
  set -x ellipsis_glyph          \uF141 # nf-fa-ellipsis-h

  set -x virtualenv_glyph        \uE73C ' '
  set -x ruby_glyph              \uE791 ' '
  set -x go_glyph                \uE626 ' '
  set -x node_glyph              \uE718 ' '
  # Git
  set -x branch_glyph            \uF418
  set -x detached_glyph          \uF417
  set -x tag_glyph               \uF412
  set -x git_dirty_glyph         \uF448 '' # nf-oct-pencil
  set -x git_staged_glyph        \uF692 '' # nf-fa-save
  set -x git_stashed_glyph       \uF0C6 '' # nf-fa-paperclip
  set -x git_untracked_glyph     $ellipsis_glyph
  set -x git_ahead_glyph         \uF47B # nf-oct-chevron_up
  set -x git_behind_glyph        \uF47C # nf-oct-chevron_down
  set -x git_plus_glyph          \uF0DE # fa-sort-asc
  set -x git_minus_glyph         \uF0DD # fa-sort-desc
  set -x git_plus_minus_glyph    \uF0DC # fa-sort
  # Cli
  set -x right_black_arrow_glyph \uE0B0
  set -x right_arrow_glyph       \uE0B1
  set -x left_black_arrow_glyph  \uE0B2
  set -x left_arrow_glyph        \uE0B3
  set -x nonzero_exit_glyph      \uF12A '' # '! '
  set -x superuser_glyph         \uF292 '' # '$ '
  set -x bg_job_glyph            \uF155 '' # '% '
  # Python
  set -x superscript_glyph       \u00B9 \u00B2 \u00B3
  set -x virtualenv_glyph        \u25F0
  set -x pypy_glyph              \u1D56
  # Desk
  set -x desk_glyph              \u25F2
  # Kubernetes
  set -x k8s_glyph               \u2388 # '⎈'
  # Vagrant
  set -x vagrant_saved_glyph     \u21E1 # ⇡ 'saved'
  set -x vagrant_stopping_glyph  \u21E3 # ⇣ 'stopping'
  set -x vagrant_running_glyph  \uF431 # ↑ 'running'
  set -x vagrant_poweroff_glyph \uF433 # ↓ 'poweroff'
  set -x vagrant_aborted_glyph  \uF468 # ✕ 'aborted'
  set -x vagrant_unknown_glyph  \uF421 # strange cases
end

function _nnix_glyph -S -a nix_env -d "Return the glyph for the environment"
  switch "$nix_env"
    case "Linux"
      set -x nix_glyph \uE712 '' # nf-dev-linux
    case "*"
      set -x nix_glyph \uF83C # nf-dev-linux
  end
end