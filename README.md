# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="30px"/> Noticia

An opinionated theme for [Oh My Fish][omf-link]. 

[![MIT License](https://img.shields.io/badge/license-MIT-blue?style=for-the-badge)](LICENSE.md)
[![Fish Shell](https://img.shields.io/badge/fish-3.1.0-blue?style=for-the-badge)](https://fishshell.com)
[![Oh My Fish](https://img.shields.io/badge/framework-Oh%20My%20Fish-blue?style=for-the-badge)](https://fishshell.com)
[![Noticia Theme](https://img.shields.io/badge/theme-Noticia-blue?style=for-the-badge)](#requirements)

## Requirements
- [Fish][fish-link] >= 3.0.0 as active shell
- [Oh My Fish][omf-link] framework installed
- A [Nerd Font][nerd-fonts] installed and enabled

## Install
```shell script
$ omf install noticia
```

## Features
- Git integration
- Powerline support
- Colors

## Screenshot
<p align="center">
    <img src="assets/screenshot-noticia.png">
</p>
<p align="center">
    <img src="assets/screenshot-win10.png">
</p>

## Notes
The theme is based upon the [bobthefish][bobthefish] theme.

# License
[MIT][mit] © [Marc-André Appel][author]

[mit]:        https://opensource.org/licenses/MIT
[author]:     https://gitlab.com/marc-andre
[omf-link]:   https://www.github.com/oh-my-fish/oh-my-fish
[nerd-fonts]: https://www.nerdfonts.com
[bobthefish]: https://github.com/oh-my-fish/theme-bobthefish
[fish-link]:  https://www.fishshell.com
